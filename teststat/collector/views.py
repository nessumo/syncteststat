from django.http import HttpResponse
import urllib2,base64

from models import Build,TestReport,RED_STATUSES
from django.shortcuts import render
from django.utils.simplejson import dumps

JENKINS_URL = "http://192.168.1.102:8080"
JENKINS_USER = "tomwar"
JENKINS_USER_TOKEN = "bc449ed1711c597ffe5a570e2d2186cd"

def collectLast(request, projectName, buildNumber, lastCount):
    buildNumber = int(buildNumber)
    lastCount = int(lastCount)
    buildsToCollect = dumps(range(buildNumber, buildNumber-lastCount, -1))
    return render(request, "collect.xhtml", {'projectName': projectName, 'buildsToCollect': buildsToCollect})

def doCollect(request, projectName, buildNumber):
    
    print "Collecting %s %s" % (projectName, buildNumber)
    
    url = "%s/job/%s/%s/api/python/?token=%s" % (JENKINS_URL, projectName, buildNumber, JENKINS_USER_TOKEN)
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % (JENKINS_USER, JENKINS_USER_TOKEN)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)
    result = urllib2.urlopen(request)
    build_data = eval(result.read())

    
    url = "%s/job/%s/%s/testReport/api/python/?token=%s" % (JENKINS_URL, projectName, buildNumber, JENKINS_USER_TOKEN)
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % (JENKINS_USER, JENKINS_USER_TOKEN)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)
    result = urllib2.urlopen(request)
    test_data = eval(result.read())

    Build.objects.filter(projectName=projectName, buildNumber=buildNumber).delete()
    build = Build()
    build.projectName = projectName
    build.buildNumber = buildNumber
    build.duration = int(build_data['duration'])
    build.timestamp = int(build_data['timestamp'])
    build.fullDisplayName = build_data['fullDisplayName']
    build.status = build_data['result']
    build.save()
    
    for suite in test_data['suites']:
        for case in suite['cases']:
            if (case['status'] in RED_STATUSES):
                className = case['className']
                index = className.rfind(".")
                if index <> -1:
                    report = TestReport()
                    report.build = build
                    report.packageName = className[:index]
                    report.className = className[index+1:]
                    report.methodName = case['name']
                    report.fullName = ".".join([report.packageName,report.className,report.methodName])
                    report.status = case['status']
                    report.duration = case['duration']
                    report.save()

    return HttpResponse("Project: %s\n Build: %s\n" % (projectName, buildNumber))