from django.db import models

RED_STATUSES = ["FAILED", "REGRESSION", "SKIPPED"]

class Build(models.Model):
    projectName = models.CharField(max_length=64)
    buildNumber = models.IntegerField()
    status = models.CharField(max_length=32)
    duration = models.BigIntegerField()
    fullDisplayName = models.CharField(max_length=64)
    timestamp = models.BigIntegerField()
    
    def __unicode__(self):
        return "%s:%s" % (self.projectName, self.buildNumber)
    
class TestReport(models.Model):
    build = models.ForeignKey(Build)
    packageName = models.CharField(max_length=384)
    className = models.CharField(max_length=128)
    methodName = models.CharField(max_length=128)
    fullName = models.CharField(max_length=512)
    status = models.CharField(max_length=32)
    duration = models.BigIntegerField()
    
    def __unicode__(self):
        return "%s.%s()" % (self.className, self.methodName)
