from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',

    url(r'^(?P<projectName>[\w.-_]+)/(?P<buildNumber>\d+)/(?P<lastCount>\d+)/$', 'collector.views.collectLast', name='collectLast'),
    url(r'^ajax/(?P<projectName>[\w.-_]+)/(?P<buildNumber>\d+)/$', 'collector.views.doCollect', name='doCollect'),

)
