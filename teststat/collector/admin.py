from models import Build, TestReport
from django.contrib import admin

class BuildAdmin(admin.ModelAdmin):
    fields = ['projectName', 'buildNumber', 'status', 'duration', 'timestamp']
    readonly_fields = fields
    list_display = ['fullDisplayName', 'projectName', 'buildNumber', 'status', 'duration', 'timestamp']
    list_display_links = ['fullDisplayName']
    list_filter = ['projectName', 'status']

admin.site.register(Build, BuildAdmin)

class TestReportAdmin(admin.ModelAdmin):
    fields = ['build', 'className', 'methodName', 'status']
    readonly_fields = fields
    list_display = ['build', 'className', 'methodName', 'status']
    list_display_links = ['methodName']
    list_filter = ['build__projectName', 'status']
    search_fields = ['methodName', 'className']

admin.site.register(TestReport, TestReportAdmin)
