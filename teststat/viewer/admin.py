from django.contrib import admin
from viewer.models import BuildSubGroup, BuildGroup

class BuildSubGroupInline(admin.StackedInline):
    model = BuildSubGroup

class BuildGroupAdmin(admin.ModelAdmin):
    list_display = ['prefix', 'listSubGroups']
    inlines = [BuildSubGroupInline]

admin.site.register(BuildGroup, BuildGroupAdmin)
