from django.conf.urls.defaults import patterns, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',

    url(r'^ajax/randomFails/results/(?P<projectNames>[\w,.-_]*)/(?P<minRanTests>\d+)/(?P<maxFailedTests>\d+)/(?P<maxBuilds>\d+)/$', 'viewer.views.randomFailsResults', name='randomFailsResults'),
    url(r'^randomFails/$', 'viewer.views.randomFails', name='randomFails'),

)

urlpatterns += staticfiles_urlpatterns()
    