from django.db import models

class BuildGroup(models.Model):
    prefix = models.CharField(max_length=10)
    
    def listSubGroups(self):
        subs = BuildSubGroup.objects.filter(group=self)
        return ", ".join([unicode(sub) for sub in subs])
    
    def __unicode__(self):
        return self.prefix
    
class BuildSubGroup(models.Model):
    group = models.ForeignKey(BuildGroup, related_name='subgroups')
    infix = models.CharField(max_length=10)
    
    def __unicode__(self):
        return self.infix
