from collector.models import Build, TestReport
from django.shortcuts import render
from viewer.models import BuildGroup, BuildSubGroup


def randomFails(request):
    minRanTests = int(request.GET.get('minRanTests',1000))
    maxFailedTests = int(request.GET.get('maxFailedTests',10))
    maxBuilds = int(request.GET.get('maxBuilds',50))
    projectNames = request.GET.getlist('projectNames')

    minRanTestsOptions = [10, 100, 500, 1000, 2000]
    if not minRanTests in minRanTestsOptions:
        minRanTestsOptions.append(minRanTests)
        minRanTestsOptions = sorted(minRanTestsOptions)

    maxFailedTestsOptions = [5, 10, 25, 50]
    if not maxFailedTests in maxFailedTestsOptions:
        maxFailedTestsOptions.append(maxFailedTests)
        maxFailedTestsOptions = sorted(maxFailedTestsOptions)
    
    maxBuildsOptions = [10,25,50,100]
    if not maxBuilds in maxBuildsOptions:
        maxBuildsOptions.append(maxBuilds)
        maxBuildsOptions = sorted(maxBuildsOptions)
    
    projectNameOptions = groupProjectNames([str(result['projectName']) for result in Build.objects.values('projectName').distinct()])

    return render(request,
                  "randomFails.xhtml", {
                        'projectNames': projectNames,
                        'minRanTests': minRanTests,
                        'maxFailedTests': maxFailedTests,
                        'maxBuilds': maxBuilds,
                        'projectNameOptions': projectNameOptions,
                        'minRanTestsOptions': minRanTestsOptions,
                        'maxFailedTestsOptions': maxFailedTestsOptions,
                        'maxBuildsOptions': maxBuildsOptions,
                        })

def groupProjectNames(names):
    result = {}
    groups = BuildGroup.objects.select_related().all()
    for name in names:
        build_group = None
        build_subgroup = None
        for group in groups:
            if name.startswith(group.prefix):
                build_group = group.prefix
                for subgroup in group.subgroups.all():
                    if name.find(subgroup.infix) > -1:
                        build_subgroup = subgroup.infix
                        break
                break
        if build_subgroup is None:
            build_subgroup = 'other'
        if build_group is None:
            build_group = 'other'
        if not result.has_key(build_group):
            result[build_group] = {}
        if not result[build_group].has_key(build_subgroup):
            result[build_group][build_subgroup] = []
        result[build_group][build_subgroup].append(name)
    return result             
    

def randomFailsResults(request, projectNames, minRanTests, maxFailedTests, maxBuilds):
    
    minRanTests = int(minRanTests)
    maxFailedTests = int(maxFailedTests)
    maxBuilds = int(maxBuilds)
    projectNames = projectNames.split(",")[:5]

    builds = Build.objects.filter(projectName__in=projectNames).order_by("-timestamp")[:maxBuilds]
    
    failedTestNames = set()
    blackBuilds = []
    greenBuilds = []
    
    for build in builds:
        failedReports = TestReport.objects.filter(build=build)
        # FIXME: we must save ran tests count as a build property in collector
        allCount = 999999
        failedCount = failedReports.count()
        if failedCount == 0:
            if allCount < minRanTests:
                blackBuilds.append(build)
            else:
                greenBuilds.append(build)
        elif failedCount > maxFailedTests:
            blackBuilds.append(build)
        else:
            for report in failedReports:
                failedTestNames.add(report.fullName)
    
    testResults = {}
    buildData = []
    projectsLegend = []
    for projectName in projectNames:
        projectsLegend.append([projectName, projectNames.index(projectName)])
    for name in sorted(failedTestNames):
        testResults[name] = []
    for build in builds:
        data = {}
        data['projectName'] = build.projectName
        data['colorId'] = projectNames.index(build.projectName)
        data['buildNumber'] = build.buildNumber
        data['buildNumberShort'] = build.buildNumber % 100
        data['fullDisplayName'] = build.fullDisplayName
        if build in blackBuilds:
            data['status'] = "BLACK"
        elif build in greenBuilds:
            data['status'] = "GREEN"
        else:
            data['status'] = "UNSTABLE"
        buildData.append(data)
        if build in blackBuilds:
            for name in failedTestNames:
                testResults[name].append({'status': "BLACK"})
            continue
        elif build in greenBuilds:
            for name in failedTestNames:
                testResults[name].append({'status': "GREEN"})
            continue
        reports = TestReport.objects.filter(build=build)
        visited = []
        for report in reports:
            if not report.fullName in visited:
                testResults[report.fullName].append(report)
                visited.append(report.fullName)
        for name in failedTestNames:
            if not name in visited:
                testResults[name].append({'status': "PASSED"})
    testData = []
    for key,value in testResults.iteritems():
        tokens = key.split(".")
        shortName = ".".join(tokens[-2:])
        data = {'shortName': shortName, 'fullName': key, 'build': build, 'results': value}
        testData.append(data)

    return render(request, "results.xhtml", {'testData': testData, 'buildData': buildData, 'projectsLegend': projectsLegend, 'jenkinsUrl': "http://jenkins:8080", 'buildsCount': len(testData)})
