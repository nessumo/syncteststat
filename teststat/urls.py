from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', direct_to_template, {'template': 'home.xhtml'}),
    url(r'^collector/', include('teststat.collector.urls')),
    url(r'^viewer/', include('teststat.viewer.urls')),

    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
